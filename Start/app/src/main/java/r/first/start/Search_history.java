package r.first.start;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class Search_history extends android.support.v4.app.Fragment {
    AlertDialog alertDialog;
    private SharedPreferences saveHistory,returnHistory;
    private ListView listView;
    private Button search_button,show_button;
    private TextView txt;
    private Context ctx;
    private View mView;
    String aa;
    public ArrayList<String> history= new ArrayList<>();
    public String [] hist_array={"","","","","",""};

    public Search_history() {
        // Required empty public constructor
    }

    public String[] arr(String a)//для сортировки истории
    {
        if (history.size()!=4){
            history.add(a);
        }
        if(history.size()==4){

            for (int j=0;j<3;j++) {
                for (int i = 0; i < 3; i++) {
                    aa = history.get(i + 1);
                    history.set(i + 1, history.get(i));
                    history.set(i, aa);
                }
            }

            history.set(0, a);
        }

         return history.toArray(new String[history.size()]);
    }

    public void WriteToMemory(String[] a)/*Метод для сохранения истории в память устройства*/
    {

        saveHistory= PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor =saveHistory.edit();
        String[] res =a;

        for(int i=0;i<res.length-1;i++)
            editor.putString(Integer.toString(i), res[i]);
        editor.apply();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       mView= inflater.inflate(R.layout.fragment_history, container, false);
       return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ctx=this.getContext();

        returnHistory= PreferenceManager.getDefaultSharedPreferences(getActivity());


        try {
            for(int i=0;i<hist_array.length-1;i++)
                hist_array[i]=returnHistory.getString(Integer.toString(i), " ");
        }
        catch (Exception e){
            Log.println(Log.ERROR, "TRY/CATCH", "ЗАПИСЬ НЕ СРАБОТАЛА БЛЯ");
        }

        listView=(ListView) mView.findViewById(R.id.history_list);
        search_button=(Button) mView.findViewById(R.id.HSbutton) ;
        txt=(TextView) mView.findViewById(R.id.HSeditText);
        show_button=(Button) mView.findViewById(R.id.show_me);



        ArrayAdapter<String> hist = new ArrayAdapter<String>(ctx,android.R.layout.simple_list_item_1 ,hist_array);
        listView.setAdapter(hist);


        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search_category = txt.getText().toString();
                hist_array=arr(search_category);
                WriteToMemory(hist_array);
                String type = "search";
                String res = "";
                backgound_worker backgound_worker = new backgound_worker(getActivity());
                backgound_worker.execute(type, search_category);

                try {


                    if (backgound_worker.get().equals("find")) {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                    if (backgound_worker.get().equals("nofind")) {

                        alertDialog = new AlertDialog.Builder(getActivity()).create();
                        alertDialog.setTitle("Fail");
                        alertDialog.setMessage("Not Find");
                        alertDialog.show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });

        show_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Array");
                alertDialog.setMessage(hist_array[0]);
                alertDialog.show();

                for(int i=0;i<hist_array.length;i++){
                     System.out.print(hist_array[i]);
                }



            }
        });



    }
}

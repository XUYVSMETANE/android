package r.first.start;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

     private BottomNavigationView mMainNavigation;

    private FrameLayout mMainFrame;
    TextView view;
    private SharedPreferences a;
    private Home homeFragment;
    private Map mapFragment;
    private  Settings settingsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        homeFragment= new Home();
        mapFragment = new Map();

        settingsFragment = new Settings();


        mMainNavigation=findViewById(R.id.MainNav);
        mMainFrame= findViewById(R.id.mFrame);
        setFragment(homeFragment);


        mMainNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){

                    case R.id.home_nav:
                        setFragment(homeFragment);
                        return true;

                    case R.id.settings_nav:
                        setFragment(settingsFragment);
                        return true;

                    case R.id.map_nav:
                        setFragment(mapFragment);
                        return true;

                    default:
                        return false;

                }
            }
        });

        Bundle bundle= getIntent().getExtras();
        if(bundle!=null){
            if(bundle.getString("some")!=null){

                Toast.makeText(getApplicationContext(),"data:"+bundle.getString("some"),Toast.LENGTH_SHORT).show();

            }

        }

    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction= getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mFrame,fragment);
        fragmentTransaction.commit();
    }

}

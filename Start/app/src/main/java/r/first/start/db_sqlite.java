package r.first.start;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class db_sqlite extends SQLiteOpenHelper {

    public static String tableName = "History";
    public static String zapros="zapros";
    private static final int DATABASE_VERSION = 1;

    public db_sqlite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE tableName(_id INTEGER PRIMARY KEY AUTOINCREMENT , zapros TEXT);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

                db.execSQL("DROP TABLE IF EXISTS tableName");
                onCreate(db);

    }
}

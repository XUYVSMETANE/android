package r.first.start;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class backgound_worker extends AsyncTask<String,Void,String> {

    Context context;
   // AlertDialog alertDialog;
     backgound_worker (Context ctx){
        context =ctx;
    }
    @Override
    protected String doInBackground(String... voids) {
         String type=voids[0];
         String search_url="http://192.168.0.54/search.php";
         if(type.equals("search")){                                         //Начало обработки и отправки запроса поиска
             try {
                 String search_category=voids[1];
                 URL url = new URL(search_url);                             //Адрес подключения
                 HttpURLConnection httpURlConnection = (HttpURLConnection)url.openConnection();  //Открытие соединения
                 httpURlConnection.setRequestMethod("POST");                                     //Метод запроса POST для отправки данных на сервер
                 httpURlConnection.setDoOutput(true);                                            //URL используется для вывода(POST)
                 httpURlConnection.setDoInput(true);                                             //Получение ответа по умолчанию
//gaaaa

                 //НАЧАЛО ЗАПИСИ ЗАПРОСА ПОЛЬЗОВАТЕЛЯ
                 OutputStream outputStream= httpURlConnection.getOutputStream();                 //Получение выходного потока
                 Writer writer= new OutputStreamWriter(outputStream,"UTF-8");        // Создание writer'a на основе выходного потока для реализации символьной записи
                BufferedWriter bufferedWriter = new BufferedWriter(writer);                     //Создание буферизированного writer'a
                 String post_data= URLEncoder.encode("search_category","UTF-8")
                         +"="+URLEncoder.encode(search_category, "UTF-8");                //Кодировка запроса пользователя
                 bufferedWriter.write(URLDecoder.decode(post_data, "UTF-8"));                                                //Буферизированный выходной символьный поток
                 bufferedWriter.flush();                                                         //Очищение буферов выходного потока записи
                 bufferedWriter.close();                                                         //закрытие потока записиа

                 outputStream.close();                                                           //Закрытие выходного поткоа

                 //ПОЛУЧЕНИЕ ОТВЕТА ОТ СЕРВЕРА И ВЫВОД РЕЗУЛЬТАТА О ПОДКЛЮЧЕНИИ И ПОИСКА
                 InputStream inputStream =httpURlConnection.getInputStream();                    //Входной поток
                 Reader reader = new InputStreamReader(inputStream,"UTF-8");        //Создание reader'а на основе входного потока для реализации символьного вывода
                 BufferedReader bufferedReader =new BufferedReader(reader);                      //Создание буферного reader'a
                 String result="";                                                               //Результат покдлючения и поиска(берется из search.php)
                 String line="";
                 while((line=bufferedReader.readLine())!=null){                                  //Запись в строку из буферного reader'a для записи результата
                     result+=line;
                 }
                 bufferedReader.close();                                                          //закрытие буферного reader'a
                 inputStream.close();                                                             // Закрытие входного потока
                 httpURlConnection.disconnect();                                                  // Закрытие Покдлючения к серверу
                 return result;


             } catch (MalformedURLException e) {
                 e.printStackTrace();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
        return null;
    }

    @Override
    protected void onPreExecute() {
        /* alertDialog=new AlertDialog.Builder(context).create();
         alertDialog.setTitle("Status");*/

    }

    @Override
    protected void onPostExecute(String result) {

       // alertDialog.setMessage(result);
        //alertDialog.show();

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}


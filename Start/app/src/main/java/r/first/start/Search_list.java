package r.first.start;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

public class Search_list extends android.support.v4.app.Fragment {

    private ListView listView;
    AlertDialog alertDialog;
    String r;
    private Button srch;
    private TextView txt;
    private View mView;


    String[] search_items={"Один","Два","Три","Четыре"};
    Integer[] ImgForSearchList={R.drawable.h1,
            R.drawable.h2,
            R.drawable.h3,
            R.drawable.h4,
            R.drawable.h5};


    public Search_list() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.search_fragment, container, false);

        return mView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listView=(ListView) mView.findViewById(R.id.search_list);
        txt=(TextView) mView.findViewById(R.id.editText);
        srch=(Button) mView.findViewById(R.id.button);



        CustomListView customListView =new CustomListView(getActivity(),search_items,ImgForSearchList);
        listView.setAdapter(customListView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent= new Intent(getActivity(),MainActivity.class);
                startActivity(intent);
            }
        });

        srch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search_category = txt.getText().toString();
                history_search hs = new history_search();
                hs.setZapros(search_category);
                String type = "search";
                String res = "";
                backgound_worker backgound_worker = new backgound_worker(getActivity());
                backgound_worker.execute(type, search_category);

                try {


                    if (backgound_worker.get().equals("find")) {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                    if (backgound_worker.get().equals("nofind")) {

                        alertDialog = new AlertDialog.Builder(getActivity()).create();
                        alertDialog.setTitle("Fail");
                        alertDialog.setMessage("Not Find");
                        alertDialog.show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


            }
        });
    }
}


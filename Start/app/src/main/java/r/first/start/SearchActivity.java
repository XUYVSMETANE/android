package r.first.start;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.security.AccessController;
import java.util.concurrent.ExecutionException;

public class SearchActivity extends AppCompatActivity{


    private static final int SWIPE_MIN_DISTANCE = 130;
    private static final int SWIPE_MAX_DISTANCE = 300;
    private static final int SWIPE_MIN_VELOCITY = 2;
    private String swipePos="Left";
    private GestureDetectorCompat lSwipeDetector;
   private Search_list search_list;
   private Search_history search_history;
   private FrameLayout sFrame;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        lSwipeDetector = new GestureDetectorCompat(this, new MyGestureListener());
        search_list= new Search_list();
        search_history = new Search_history();
        sFrame= findViewById(R.id.sFrame);
         setFragment(search_list);
            sFrame.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return lSwipeDetector.onTouchEvent(event);
                }
            });


    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction= getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.sFrame,fragment);
        fragmentTransaction.commit();

    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_DISTANCE)
                return false;
            if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_MIN_VELOCITY) {
                    /*if(swipePos.equals("Left")){
                        setFragment(search_list);
                        swipePos="Right";
                    }
                    else {*/
                        setFragment(search_list);
                        swipePos="Left";
                    //}
            }
            if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_MIN_VELOCITY){
              /* if(swipePos.equals("Right")){
                    setFragment(search_list);
                    swipePos="Left";
                }
                else{*/
                   setFragment(search_history);
                   swipePos="Right";
               //}
            }
            return false;
        }
    }

}

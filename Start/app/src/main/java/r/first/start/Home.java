package r.first.start;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment implements OnMapReadyCallback {


    private GoogleMap mMap;
    private MapView mapView;
    private View mView;
    Button search;
    public Home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.fragment_home, container, false);

        search=(Button) mView.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity(),test.class);

                intent.putExtra("some","some pidorasi");
                startActivity(intent);
            }
        });

        return mView;





    }


    @Override
    public void onViewCreated(@NonNull View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView=(MapView) mView.findViewById(R.id.mapView);
        search=(Button) search.findViewById(R.id.search);
        View.OnClickListener onClickListener= new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){

                    case R.id.search:
                        Intent intent= new Intent(getContext(),SearchActivity.class);
                        startActivity(intent);
                        break;


                }
            }
        };

        search.setOnClickListener(onClickListener);


        if(mapView!=null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);

        }
    }








    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(getContext());
        mMap= googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);



        // Add a marker in Sydney and move the camera
        LatLng moscow = new LatLng(55, 37);
        mMap.addMarker(new MarkerOptions().position(moscow).title("Marker in Sydney"));
        CameraPosition Sydney = CameraPosition.builder().target(moscow).build();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(moscow));

    }
}

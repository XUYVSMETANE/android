package r.first.start;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListView extends ArrayAdapter<String> {

    private String[] search_items;
    private Integer[] ImgForSearchList;
    private Activity context;



    public CustomListView(Activity context, String[] search_items, Integer[] ImgForSearchList) {
        super(context, R.layout.search_list,search_items);

        this.context=context;
        this.search_items=search_items;
        this.ImgForSearchList=ImgForSearchList;


    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View r= convertView;
        ViewHolder viewHolder= null;

        if(r==null){

            LayoutInflater layoutInflater=context.getLayoutInflater();
            r=layoutInflater.inflate(R.layout.search_list,parent,false);
            viewHolder= new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else{
            viewHolder=(ViewHolder) r.getTag();


        }

        viewHolder.img.setImageResource(ImgForSearchList[position]);
        viewHolder.txt1.setText(search_items[position]);



        return r;


    }

    class ViewHolder {

        TextView txt1;

        ImageView img;

        ViewHolder(View v){

            txt1=v.findViewById(R.id.txt_search);
            img=v.findViewById(R.id.icon_search);
        }
    }
}

